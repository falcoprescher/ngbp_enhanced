module.exports = function (grunt) {
  'use strict';

  var path = require('path');

  /**
   * Load required Grunt tasks. These are installed based on the versions listed
   * in `package.json` when you do `npm install` in this directory.
   */
  require('load-grunt-config')(grunt, {
    configPath: path.join(process.cwd(), 'grunt/config'),

    /**
     * Load in our build configuration file.
     */
    data: require('./build.config.js')
  });
  grunt.loadTasks('grunt/tasks');
};
