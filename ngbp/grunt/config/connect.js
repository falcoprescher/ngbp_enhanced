module.exports = {
  options: {
    hostname: 'localhost',
    port: 8080,
    open: true,
    middleware: function (connect, options, middlewares) {
      middlewares.unshift(function addCspHeaders(req, res, next) {
        var policy = 'style-src \'self\'; script-src \'self\'; font-src \'self\'; connect-src \'self\'; object-src \'none\';';

        //res.setHeader('Content-Security-Policy', policy);

        next();
      });

      return middlewares;
    }
  },

  dev: {
    options: {
      base: ['./build'],
      index: 'index.html',
      debug: true
    }
  },

  prod: {
    options: {
      base: ['./bin'],
      index: 'index.html',
      keepalive: true
    }
  }
};