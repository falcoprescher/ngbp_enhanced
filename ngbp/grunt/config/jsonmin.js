module.exports = {
  compile_i18n: {
    options: {
      stripWhitespace: true,
      stripComments: true
    },
    files: {
      'bin/i18n/locale-de.json': 'i18n/locale-de.json',
      'bin/i18n/locale-en.json': 'i18n/locale-en.json'
    }
  }
};