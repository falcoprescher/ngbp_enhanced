/**
 * @doc module
 * @name docular-test
 * @description This module contains all the logic for the workflow of generating
 * partials and front end resources for display of docular documentation.
 *
 * This is a comment BLOCK that will be in the documentation
 * This same information should probably be hidden during development
 */
angular.module('ngBoilerplate.about', [
  'ui.router',
  'placeholders',
  'ui.bootstrap'
])

.config(['$stateProvider', function config($stateProvider) {
  $stateProvider.state('about', {
    url: '/about',
    views: {
      main: {
        controller: 'AboutCtrl',
        templateUrl: 'about/about.tpl.html'
      }
    },
    data: {
      pageTitle: 'What is It?',
      ncyBreadcrumbLabel: 'About'
    }
  });
}])

.controller('AboutCtrl', ['$scope', function AboutCtrl($scope) {
  // This is simple a demo for UI Boostrap.
  $scope.dropdownDemoItems = [
    'The first choice!',
    'And another choice for you.',
    'but wait! A third!'
  ];
}]);
