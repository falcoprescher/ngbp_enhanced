module.exports = {
  useHtml5Mode: false, //Use angular's html5 mode? true/false.
  docular_webapp_target: 'doc', //The place where the doc will be generated
  showAngularDocs: true,
  showDocularDocs: true,
  examples: {}, //instructions for how to run the sandboxed examples
  groups: [{
    groupTitle: 'ngbp_enhanced Docs', //Title used in the UI
    groupId: 'src', //identifier and determines directory
    groupIcon: 'icon-book', //Icon to use for this group
    sections: [
      {
        id: 'boilerplate',
        title: 'My API',
        scripts: ['src/app']
      }
    ]
  }] //groups of documentation to parse
};