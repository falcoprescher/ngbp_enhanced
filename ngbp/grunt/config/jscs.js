module.exports = {
  src: ['<%= app_files.js %>', '<%= app_files.jsunit %>', 'Gruntfile.js'],
  options: {
    config: '.jscsrc'
  }
};